.PHONY: clean system-packages python-packages install run all

clean:
	find . -type f -name '*.pyc' -delete
	find . -type f -name '*.log' -delete

system-packages:
	virtualenv boostupenv

python-packages:
	. boostupenv/bin/activate
	env LDFLAGS='-L/usr/local/lib -L/usr/local/opt/openssl/lib -L/usr/local/opt/readline/lib' pip install psycopg2
	pip install -r requirements.txt

install: system-packages python-packages

run:
	. boostupenv/bin/activate
	python manage.py db upgrade
	python manage.py run

all: clean install run