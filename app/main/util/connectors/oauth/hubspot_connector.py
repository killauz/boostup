import urllib
import requests

from flask import current_app
from hubspot3.error import HubspotUnauthorized, HubspotNotFound
from hubspot3.deals import DealsClient

from .exceptions import UnauthorizedException, NotFoundException
from app.main.util.connectors.oauth.base_connector import OauthBaseConnector


class HubspotOauthConnector(OauthBaseConnector):

    def retrieve_credentials(self, code):
        url = '{base_url}/oauth/v1/token'.format(
            base_url=current_app.config['HUBSPOT_API_URL'])
        redirect_uri = '{base_url}/{endpoint}'.format(
                base_url=current_app.config['HUBSPOT_REDIRECT_URI'],
                endpoint='session')

        params = {
            'grant_type': 'authorization_code',
            'client_id': current_app.config['HUBSPOT_CLIENT_ID'],
            'client_secret': current_app.config['HUBSPOT_SECRET'],
            'redirect_uri': redirect_uri,
            'code': code
        }

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        response = requests.post(url, data=params)
        return response.json()

    def get_deals(self, access_token, limit, offset):
        try:
            client = DealsClient(
                access_token=access_token
            )
            return client.get_all(limit=int(limit), offset=int(offset))
        except HubspotUnauthorized:
            raise UnauthorizedException

    def get_deal_by_id(self, access_token, deal_id):
        try:
            client = DealsClient(
                access_token=access_token
            )
            return client.get(deal_id)
        except HubspotNotFound:
            raise NotFoundException
        except HubspotUnauthorized:
            raise UnauthorizedException
