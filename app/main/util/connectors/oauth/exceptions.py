class UnauthorizedException(Exception):
    pass

class NotFoundException(Exception):
    pass