from abc import ABC, abstractmethod


class OauthBaseConnector(ABC):

    @abstractmethod
    def retrieve_credentials(self, code):
        pass
