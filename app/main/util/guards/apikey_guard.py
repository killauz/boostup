from functools import wraps

from flask import request, abort

from app.main.model.apikey import Apikey

# The actual decorator function
def require_apikey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        provided_apikey = request.values.get('apikey')
        apikey = Apikey.query.filter_by(api_key=provided_apikey).first()
        if not apikey:
            abort(401, "Missing or invalid api key")
        return view_function(*args, **kwargs)
    return decorated_function