from flask_restplus import Namespace, fields


class AuthDto:
    api = Namespace('auth', description='auth ops')
    auth = api.model('auth', {
        'user_id': fields.String(required=True, description='user id'),
        'access_code': fields.String(required=True, description='access code'),
        'refresh_token': fields.String(required=True, description='refresh token')
    })

    session = api.model('session', {
        'code': fields.String(required=True, description='code')
    })


class DealsDto:
    api = Namespace('deals', description='deals ops')
    deal = api.model('deal', {
        'name': fields.String(description='user email address'),
        'stage': fields.String(description='user username'),
        'close_date': fields.String(description='user password'),
        'amount': fields.String(description='user Identifier'),
        'original_type': fields.String(description='user Identifier')
    })