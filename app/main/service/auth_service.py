
from flask import current_app

from .. import db
from ..model.auth import Auth
from ..util.connectors.oauth.hubspot_connector import HubspotOauthConnector


class AuthService(object):

    def generate_login_url(self):
        response = '{base_url}/oauth/authorize?client_id={client_id}&scope=contacts&redirect_uri={redirect_uri}/session'
        return response.format(
            base_url=current_app.config['HUBSPOT_BASE_URL'],
            client_id=current_app.config['HUBSPOT_CLIENT_ID'],
            redirect_uri=current_app.config['HUBSPOT_REDIRECT_URI'])

    def save_new_session(self, code):
        client_id = current_app.config['HUBSPOT_CLIENT_ID']
        session = Auth.query.filter_by(client_id=client_id).first()
        if not session:
            connector = HubspotOauthConnector()
            credentials = connector.retrieve_credentials(code)

            new_session = Auth(
                client_id=client_id,
                access_code=credentials.get('access_token'),
                refresh_token=credentials.get('refresh_token')
            )
            db.session.add(new_session)
            db.session.commit()
        else:
            raise Exception('Session already exists.')


    def get_current_session(self, client_id=None):
        try:
            if not client_id:
                client_id = current_app.config['HUBSPOT_CLIENT_ID']
            response = Auth.query.filter_by(client_id=client_id).first()
            return response.access_code
        except:
            raise Exception('Session not found, please connect your account again.')

    def expire_session(self, client_id=None):
        try:
            if not client_id:
                client_id = current_app.config['HUBSPOT_CLIENT_ID']
            response = Auth.query.filter_by(client_id=client_id).first()
            db.session.delete(response)
            db.session.commit()
        except:
            raise Exception('Session not found, please connect your account again.')

