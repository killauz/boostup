import uuid
import datetime

from flask import current_app

from .auth_service import AuthService
from .exceptions import UnauthorizedService
from ..model.deal import Deal
from ..util.connectors.oauth.exceptions import UnauthorizedException, NotFoundException
from ..util.connectors.oauth.hubspot_connector import HubspotOauthConnector
from .. import db


class DealService(object):
    def __init__(self):
       self.hub_client = HubspotOauthConnector()
       self.auth_service = AuthService()

    def _get_current_session(self):
        try:
            return self.auth_service.get_current_session()
        except:
            raise UnauthorizedService('No session, please log in again.')

    @staticmethod
    def _parse_deals_data(deal):
        return {
            "id": deal.get("id"),
            "name": deal.get("dealname"),
            "stage": deal.get("dealstage"),
            "close_date": deal.get("closedate"),
            "amount": deal.get("amount"),
            "original_type": deal.get("dealtype")
        }

    @staticmethod
    def _get_single_deal_property(properties, key):
        return properties.get(key, {}).get("value")


    def _create_deal_from_data(self, data):
        return Deal(
            original_id=data["id"],
            name=data["name"],
            stage=data["stage"],
            close_date=data["close_date"],
            amount=data["amount"],
            original_type=data["original_type"]
        )

    def _parse_single_deal_data(self, deal):
        properties = deal.get("properties", {})
        return {
            "id": deal.get("dealId"),
            "name": self._get_single_deal_property(properties, "dealname"),
            "stage": self._get_single_deal_property(properties, "dealstage"),
            "close_date": self._get_single_deal_property(properties, "closedate"),
            "amount": self._get_single_deal_property(properties, "amount"),
            "original_type": self._get_single_deal_property(properties, "dealtype")
        }

    def get_all_deals(self, limit, offset):
        access_token = self._get_current_session()
        try:
            deals = self.hub_client.get_deals(access_token, limit, offset)
            return [self._parse_deals_data(deal) for deal in deals]

        except UnauthorizedException as exc:
            self.auth_service.expire_session()
            raise UnauthorizedService

    def get_deal_by_id(self, deal_id, force_refresh=False):
        stored_deal = Deal.query.filter_by(original_id=deal_id).first()
        if stored_deal and not force_refresh:
            return stored_deal
        else:
            access_token = self._get_current_session()
            try:
                deal = self.hub_client.get_deal_by_id(access_token, deal_id)
                if deal:
                    deal_data = self._parse_single_deal_data(deal)
                    if stored_deal:
                        stored_deal.name = deal_data['name']
                        stored_deal.stage = deal_data['stage']
                        stored_deal.close_date = deal_data['close_date']
                        stored_deal.amount = deal_data['amount']
                        stored_deal.original_type = deal_data['original_type']
                        db.session.commit()
                        return stored_deal
                    else:
                        new_deal = self._create_deal_from_data(deal_data)
                        db.session.add(new_deal)
                        db.session.commit()
                        return new_deal
            except NotFoundException:
                return None
            except UnauthorizedException as exc:
                self.auth_service.expire_session()
                raise UnauthorizedService
