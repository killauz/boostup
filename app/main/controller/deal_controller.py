from flask import request
from flask_restplus import Resource

from ..util.dto import DealsDto
from ..service import exceptions as service_exceptions
from ..service.deal_service import DealService
from ..util.guards.apikey_guard import require_apikey

api = DealsDto.api

@api.route('/')
class Deals(Resource):
    dealService = DealService()

    @require_apikey
    def get(self):
        try:
            limit = request.values.get('limit', 10)
            offset = request.values.get('offset', 0)

            response = {
                "data": self.dealService.get_all_deals(limit=limit, offset=offset)
            }
            return response, 200
        except service_exceptions.UnauthorizedService:
            api.abort(401, "Hubspot session not found, please authenticate again.")

@api.route('/<deal_id>')
@api.param('deal_id', 'The Deal identifier')
class Deal(Resource):
    dealService = DealService()

    @api.marshal_list_with(DealsDto.deal, envelope='data')
    @require_apikey
    def get(self, deal_id):
        force_refresh = request.values.get('force_refresh', '') == 'true'
        try:
            deal = self.dealService.get_deal_by_id(deal_id, force_refresh)
            if not deal:
                api.abort(404, "Deal not found.")
            else:
                return deal, 200
        except service_exceptions.UnauthorizedService:
            api.abort(401, "Hubspot session not found, please authenticate again.")
