from flask import request
from flask_restplus import Resource

from ..util.dto import AuthDto
from ..service.auth_service import AuthService

api = AuthDto.api
_auth = AuthDto.auth
_session = AuthDto.session


@api.route('/redirection')
class Redirect(Resource):
    authService = AuthService()

    @api.doc('list_of_registered_users')
    def get(self):
        response = {
            "data": {
                "redirection_url": self.authService.generate_login_url()
            }
        }
        return response, 200


@api.route('/session')
class User(Resource):
    authService = AuthService()

    @api.response(201, 'Session successfully created.')
    @api.doc('create a new session')
    def get(self):
        """Creates a new User """
        if 'code' not in request.values.keys():
            response = {
                "data": {
                    "error": "missing code on request query parameters"
                }
            }
            response_code = 400
        else:
            try:
                self.authService.save_new_session(code=request.values['code'])
                response = {
                    "data": {
                        "message": "connection succeeded."
                    }
                }
                response_code = 200
            except Exception as exc:
                response = str(exc)
                response_code = 500
        return response, response_code
