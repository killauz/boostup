import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    DEBUG = True
    HUBSPOT_API_URL = 'https://api.hubapi.com'
    HUBSPOT_BASE_URL = 'https://app.hubspot.com'
    HUBSPOT_CLIENT_ID = 'a9bc9ce9-ef92-463b-af5f-61a91d7ebe6c'
    HUBSPOT_SECRET = 'ded69619-8a40-4a58-8039-e8333e3f2a6c'
    HUBSPOT_REDIRECT_URI = 'http://localhost:8000/auth'
    HUBSPOT_TOKEN_TYPE = 'shared'


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL') or "postgresql:///boostup_db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


CONFIG_BY_NAME = {
    'dev': DevelopmentConfig
}
