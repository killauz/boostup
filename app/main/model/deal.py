from .. import db


class Deal(db.Model):
    __tablename__ = "deal"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    original_id = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    stage = db.Column(db.String(255), nullable=False)
    close_date = db.Column(db.String(255), nullable=False)
    amount = db.Column(db.String(255), nullable=False)
    original_type = db.Column(db.String(255), nullable=False)
