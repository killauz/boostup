from .. import db


class Apikey(db.Model):
    __tablename__ = "apikey"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_name = db.Column(db.String(255), unique=True, nullable=True)
    api_key = db.Column(db.String(255), unique=True, nullable=True)
