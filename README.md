# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains an small integration with DEALS Api, from Hubspot.

For the purpose of the project, I decided using postgresql instead of MongoDB for a couple of reasons:

* I used Flask Restplus, which works very well with SQLAlchemy, so postgres/mysql was the fastest to be integrated in my opinion
* I already had a client to explore postgresql databases installed, so I won time there too.
* I am, to be honest, not that familiar with MongoDB ORMs yet, and as I didn't really count on the time to learn it for the test, I decided to pass on this option by now.


### How do I get set up? ###

I included three ways to start the project.

* A Zip file, which you'll have to uncompress and then run some commands listed below, but bear in mind that you will need to set up postgres locally.

* A Dockerfile, which you can build and run locally. Also, you will need to either run a postgres docker container, or start postgres locally.

* A Docker compose file which will pull an already built image, and start postgresql for you (recommended).



## Instructions: ##

### Zipfile: ###

This is the alternative if you want to run the code locally, without using docker.

It will install the dependencies within a virtualenv.

*Python3, pip, and virtualenv are required for this option.*

Download and uncompress the file provided in the email, or you can download the code from bitbucket as well.

Get into the folder and run:

```
make install
make run
```

NOTE: The service expects a postgresql server running on 127.0.0.1:5432 and a database named `boostup_db` created.

For this, you can initialize it as follows:

```
psql

create database boostup_db;
```

If everything is okay, you will see this promted in your terminal:


```
 * Debug mode: on
 * Running on http://127.0.0.1:8000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 188-938-525
```

You can test it with *CURL*:

```
curl --location --request GET 'http://localhost:8000/auth/redirection'
```

Should return the following:

```
{
    "data": {
        "redirection_url": "https://app.hubspot.com/oauth/authorize?client_id=a9bc9ce9-ef92-463b-af5f-61a91d7ebe6c&scope=contacts&redirect_uri=http://localhost:8000/auth/session"
    }
}
```


### Dockerfile ###

You can also start the service from building the docker image locally and then running it.

Of course, you will need docker to be installed on your computer, and an instance of postgresql with the same requirements as above.


```
docker build -t boostuptest .
docker run -p 8000:8000 boostuptest

```

You can check it's running, since it should return something like this:


```
➜  killauz-boostup-dd1a5e3c5607 docker run -p 8000:8000 boostuptest
 * Serving Flask app "app.main" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://0.0.0.0:8000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 178-723-332
```

Also, you can perform the same *CURL* test provided above.


### Docker compose ###

This is the easiest alternative. Of course, it also demands having docker installed and running in your computer.

**You don't need to configure postgresql on your own with this alternative**

Simply go to the uncompressed folder, and run:


```
docker-compose up
```

It will pull the image from the docker registry, and run it for you without having to build it.

Again, you can test it with 

```
curl --location --request GET 'http://localhost:8000/auth/redirection'
```





### Service description ###

The service counts basically on 4 endpoints.

Two of them are dedicated to the authentication flow, and the other two are REST endpoints to work as some kind of proxy between the client and Hubspot.

To understand the authentication flow, let's first talk about the architecture and the need it covers.

The system should provide a mechanism to store Oauth2 tokens and make them available for all the API users,
so we basically have a relational database and a table, in which we store the access tokens returned after an
oauth2 flow is completed.

*The authentication flow goes as follows:*

* A client (Probably a frontend application) first hits the `auth/redirection` endpoint, asking for a url to begin the authentication process.

* The BE returns something like this:

```
{
    "data": {
        "redirection_url": "https://app.hubspot.com/oauth/authorize?client_id=a9bc9ce9-ef92-463b-af5f-61a91d7ebe6c&scope=contacts&redirect_uri=http://localhost:8000/auth/session"
    }
}
```

* The client opens the provided url, and takes the steps provided by Hubspot.

* IMPORTANT NOTE: I am not proud of the amount of time I spent debugging the integration until I realized I should've used a testing account instead of a development one.
**So please make sure you have a Hubspot testing account before you continue**

* After the flow is completed in Hubspot, we get redirected to `http://localhost:8000/auth/session?code=$CODE` which is the second auth endpdoint.

* On this endpoint, our Backend exchanges such code for credentials and stores them in the database.

* The service will respond with something like: 

```
{
    "data": {
        "message": "connection succeeded."
    }
}
```

Which means we can close the window (the frontend app can take care of that message).

* If a connection already exists, it will reject the request with a 400 code.

* The flow is completed and our API is ready to make requests to Hubspot.



*How do we use such credentials?*

Here comes the second part of the service: using the endpoints to retrieve data.

The backend api implementation counts on two endpoints:

* Getting all available deals

* Getting a particular deal information and storing it.

Before jumping into details, I want to explain why I did this like that.

The reason is simple: The **Get all** endpoint will return data directly from Hubspot to avoid working with stale data. 
Is this the only way we could get rid of it? Of course not. But it's the simplest I've found so far.

So the first step to interact with the API should be listing all the available deals with the following request:

```
curl --location --request GET 'http://localhost:8000/deals?apikey=d78af87asffa8sf7afafh123jasddn'
```

**IMPORTANT NOTE: I've implemented a decorator wich validates the api key against a clients table, to avoid exposing the endpoint without
any kind of securitization. So please remember to include `?apikey=d78af87asffa8sf7afafh123jasddn` on every request.**

The service should respond with something like:

```
{
    "data": [
        {
            "id": 4164343744,
            "name": "Tim's Newer Deal",
            "stage": "appointmentscheduled",
            "close_date": "1409443200000",
            "amount": "60000",
            "original_type": "newbusiness"
        },
        {
            "id": 4164345825,
            "name": "Tim's Newer Deal",
            "stage": "appointmentscheduled",
            "close_date": "1409443200000",
            "amount": "60000",
            "original_type": "newbusiness"
        },
        {
            "id": 4164424591,
            "name": "Tim's Newer Deal",
            "stage": "appointmentscheduled",
            "close_date": "1409443200000",
            "amount": "60000",
            "original_type": "newbusiness"
        }
    ]
}
```

And now it's the turn for the second endpoint to take it's part.

```
curl --location --request GET 'http://localhost:8000/deals/4164343744?apikey=d78af87asffa8sf7afafh123jasddn'
```

will return:

```
{
    "data": {
        "name": "Tim's Newer Deal",
        "stage": "appointmentscheduled",
        "close_date": "1409443200000",
        "amount": "60000",
        "original_type": "newbusiness"
    }
}
```

But what is really going behind courtains is:

* The backend service is retrieving the data from our database if it exists, and if it's not, it's retrieving it from Hubspot and storing it.

NOTE: I've included a query param `?force_refresh=true` to remove the stale data and refresh it with the latest we can get from Hubspot.



```
curl --location --request GET 'http://localhost:8000/deals/4164343744?apikey=d78af87asffa8sf7afafh123jasddn&force_refresh=true'
```

So if the data changes on Hubspot, we can update our copy of it locally.


Train of thought: It would have been a million times easier to explain the sense behind these choices with a frontend, but I sincerely didn't 
have the time to do so, so I will do my best to explain how it should look for me:

A simple table with pagination (the first endpoint counts with limit and offset query params) which will display all the deals directly
from Hubspot.

Then, when clicking on a given deal, the second request is fired, and the data is persisted on our database.

A switch could be provided to set the **force_refresh** value to true and make every request refresh our local data.

If the service finds itself with authentication problems, the frontend should take you back to the authentication process by poping up a window
with the oauth2 flow.



### What I would do if I had more time ###

Here's an small summary of things I'd work on rightaway if I had the time to do so:

* An endpoint to retrieve all deals stored locally.
* Frontend application, as described above, of course.
* I'd enhance the error handling, as I am just handling some kind of errors, but not all of them.
* Integrate the refresh token authentication flow: right now, if the token expires, I just delete the session and ask the user to login again.
* Unit testing.
* Component testing.
* Deploy the code somewhere so that you don't have to run it locally to test it.
* Move the token storage to some kind of ephimeral solution as redis.
* Separate the endpoints auth/deals into two different services.
* Add static code analysis! (I didn't even run pylint on it!)
* Generate production configuration settings!

And probably a lot of things more.

So a final conclusion about the prioritization of the tasks:

You might be wondering: *If you knew you had little time, why did you spend time on making dockerfiles and composer, if it wasn't even required?*

Well, in my experience, a service can be developed, reworked and enhanced a million times, depending on the requirements changes, or the integration tools needs, etc., but something is valuable for me, is to get a way to build, ship and deploy my code so that the continuous integration can exist from the very beginning. 

The code does what it should be doing? Probably yes.

Is it the best code I've every built? There's no doubt it's not.

Can it be built and run on any computer with just two lines of code? Yes, indeed.

The next step could be just set up a github action hook, or a circleci pipeline, and get every commit running on a testing environment in the blink of an eye.





