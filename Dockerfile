# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt
RUN pip install psycopg2

# copy the content of the local src directory to the working directory
ADD app /code/app
ADD migrations /code/migrations
COPY Makefile /code/Makefile
COPY manage.py /code/manage.py
COPY wait-for-it.sh /code/wait-for-it.sh
RUN chmod +x /code/wait-for-it.sh

ENV FLASK_HOST "0.0.0.0"
ENV FLASK_PORT 8000

# command to run on container start
CMD [ "python", "manage.py", "run" ]