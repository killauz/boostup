import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_cors import CORS


from app import blueprint
from app.main import create_app, db
from app.main.model import deal
from app.main.model import auth
from app.main.model import apikey



app = create_app(os.getenv('BOOSTUP_ENV') or 'dev')
app.register_blueprint(blueprint)
app.app_context().push()
manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)
CORS(app, resources={r'*': {'origins': '*'}})


@manager.command
def run():
    app.run(host=os.getenv('FLASK_HOST') or '127.0.0.1', port=os.getenv('FLASK_PORT') or '8000')

if __name__ == '__main__':
    manager.run()
