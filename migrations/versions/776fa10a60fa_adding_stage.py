"""adding stage

Revision ID: 776fa10a60fa
Revises: ad56564a4cd5
Create Date: 2021-02-16 17:34:24.977229

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '776fa10a60fa'
down_revision = 'ad56564a4cd5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('amount')
    op.add_column('deal', sa.Column('stage', sa.String(length=255), nullable=False))
    op.create_unique_constraint(None, 'deal', ['stage'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'deal', type_='unique')
    op.drop_column('deal', 'stage')
    op.create_table('amount',
    sa.Column('id', sa.INTEGER(), autoincrement=True, nullable=False),
    sa.Column('original_id', sa.VARCHAR(length=255), autoincrement=False, nullable=False),
    sa.Column('name', sa.VARCHAR(length=255), autoincrement=False, nullable=False),
    sa.Column('close_date', sa.VARCHAR(length=255), autoincrement=False, nullable=False),
    sa.Column('amount', sa.VARCHAR(length=255), autoincrement=False, nullable=False),
    sa.Column('original_type', sa.VARCHAR(length=255), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='amount_pkey'),
    sa.UniqueConstraint('amount', name='amount_amount_key'),
    sa.UniqueConstraint('close_date', name='amount_close_date_key'),
    sa.UniqueConstraint('name', name='amount_name_key'),
    sa.UniqueConstraint('original_id', name='amount_original_id_key'),
    sa.UniqueConstraint('original_type', name='amount_original_type_key')
    )
    # ### end Alembic commands ###
